<?php
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/home', function(){
  return redirect('/profile/{user}');
});

Auth::routes();

Route::get('/tasks/show/{id}', 'TasksController@show')->name('tasks.show');
Route::get('/tasks/{task}/edit', 'TasksController@edit')->name('tasks.edit');

Route::patch('tasks/{task}', 'TasksController@update')->name('tasks.update');

Route::post('/tasks', 'TasksController@store')->name('tasks.store');

Route::get('/tasks/create', 'TasksController@create')->name('tasks.create');
Route::delete('tasks/{task}', 'TasksController@destroy')->name('tasks.destroy');
Route::get('/profile/{user}', 'ProfilesController@index')->name('profile.show');
