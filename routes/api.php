<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::middleware('auth:api')->get('/profiles/show/{id}', 'ApiProfilesController@show');

Route::get('/profiles', 'ApiProfilesController@index');

Route::get('/profiles/tasks/{id}', 'ApiTasksController@show');

Route::middleware('auth:api')->post('/profiles/tasks/update', 'ApiTasksController@update');
Route::middleware('auth:api')->delete('/profiles/tasks/{id}/destroy', 'ApiTasksController@destroy');
Route::middleware('auth:api')->get('/profiles/tasks', 'ApiTasksController@index');
Route::middleware('auth:api')->post('/profiles/tasks/create', 'ApiTasksController@create');


Route::post('/register', 'Api\AuthController@register');
Route::post('/login', 'Api\AuthController@login');
