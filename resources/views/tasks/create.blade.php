@extends('layouts.app')
@section('content')
<div class="container">
  <form action="/tasks" method="post">
      @csrf
      <div class="form-group row">
        <h2>Add new To-do</h2>
      </div>

      <div class="form-group row">
          <label for="title" class="col-md-4 col-form-label text-md-right">Create To-do</label>

          <div class="col-md-6">
              <input id="title"
              type="text"
              class="form-control @error('title') is-invalid @enderror"
              name="title" value="{{ old('title') }}" required autocomplete="title" autofocus>

              @error('title')
                  <span class="invalid-feedback" role="alert">
                      <strong>{{ $message }}</strong>
                  </span>
              @enderror
          </div>
      </div>

      <div class="form-group row">
          <label for="title" class="col-md-4 col-form-label text-md-right">To-do</label>

          <div class="col-md-6">
              <input id="todo"
              type="text"
              class="form-control @error('todo') is-invalid @enderror"
              name="todo" value="{{ old('todo') }}" required autocomplete="todo" autofocus>

              @error('todo')
                  <span class="invalid-feedback" role="alert">
                      <strong>{{ $message }}</strong>
                  </span>
              @enderror
          </div>
      </div>

      <div class="form-group row mb-0">
          <div class="col-md-6 offset-md-4">
              <button type="submit" class="btn btn-primary">
                  Add To-do
              </button>
          </div>
      </div>
  </form>
</div>
@endsection
