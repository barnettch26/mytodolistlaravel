@extends('layouts.app')
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">To-Do List</div>

                <div class="card-body">
                    <a href="/tasks/create" class="btn btn-primary mb-3">Add To-do</a>
                    <table class="table table-striped" style="table-layout:fixed;">
                      <thead>
                        <tr>
                          <td>Title</td>
                          <td colspan="3">To-do</td>
                        </tr>
                      </thead>
                      <tbody>
                        @foreach($user->tasks as $task)
                        <tr>
                          <td style="word-wrap">{{$task->title}}</td>
                          <td style="word-wrap">{{$task->todo}}</td>
                          <td>
                            <a href="{{ route('tasks.edit',$task->id) }}" class="btn btn-primary">Edit</a>
                          </td>
                          <td>
                            <form action="{{ route('tasks.destroy', $task->id) }}" method="post">
                              @csrf
                              @method('DELETE')
                              <button class="btn btn-danger" type="submit">Delete</button>
                            </form>
                          </td>

                        </tr>
                        @endforeach

                      </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
