<?php

namespace App\Http\Controllers;
use App\Task;

use Illuminate\Http\Request;

class TasksController extends Controller
{
    public function __construct(){
      $this->middleware('auth');
    }

    public function show($id){
      $task = Task::find($id);

      return $task;
    }

    public function create(){
      return view('tasks.create');
    }

    public function store(Request $request){
      $task = $request->validate([
        'title' => 'required',
        'todo' => 'required',
      ]);

      auth()->user()->tasks()->create($task);

      return redirect('/profile/' . auth()->user()->id);
    }

    public function edit($id)
    {
      $task = Task::find($id);
      return view('tasks.edit', ['task' => $task]);
    }

    public function update(Request $request, $id)
    {
      $request->validate([
        'title'=>'required',
        'todo'=>'required',
      ]);

      $task = Task::find($id);
      $task->title = $request->get('title');
      $task->todo = $request->get('todo');

      $task->save();

      return redirect('/profile/' . auth()->user()->id)->with('success', 'To-do list updated!');
    }

    public function destroy($id){
      $task = Task::findOrFail($id);
      $task->delete();

      return redirect('/profile/' . auth()->user()->id)->with('success', 'Task deleted!');
    }
}
