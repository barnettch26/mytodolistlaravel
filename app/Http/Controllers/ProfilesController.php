<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;

class ProfilesController extends Controller
{
    public function __construct(){
      $this->middleware('auth');
    }

    public function index($userId)
    {
        if(auth()->id() == $userId){
          $user = User::findOrFail($userId);
          return view('profiles.index', ['user' => $user,]);
        }
        else{
          return redirect('/profile/' . auth()->id());
        }
    }
}
