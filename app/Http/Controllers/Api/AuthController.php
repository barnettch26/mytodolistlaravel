<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\Hash;

use Illuminate\Support\Str;

class AuthController extends Controller
{
    public function register(Request $request)
    {
      $user = User::create([
          'name' => $request->input('name'),
          'email' => $request->input('email'),
          'password' => Hash::make($request->input('password')),
      ]);
      //$accessToken = Str::random(60);
      $accessToken = $user->createToken('authToken')->accessToken;
      return response(['user'=> $user, 'access_token'=>$accessToken]);
    }

    public function login(Request $request)
    {
      $data = $request->validate([
        'email'=>'email|required',
        'password'=>'required'
      ]);

      if(!auth()->attempt($data)){
        return response(['success'=>false, 'message'=>'Invalid credentials']);
      }

      $accessToken = auth()->user()->createToken('authToken')->accessToken;
      //$accessToken = auth()->user()->accessToken;

      return response([
        'success'=>true,
        'user'=>auth()->user(),
        'access_token'=>$accessToken
      ]);
    }
}
