<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Task;
use App\User;
use App\Http\Controllers\DB;

class ApiTasksController extends Controller
{
    public function __construct(){
      $this->middleware('auth:api');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $tasks = Task::where('user_id', auth()->user()->id)
              ->get();

      $response = [
        'success'=>true,
        'tasks'=>$tasks,
      ];
      return response()->json($response);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
      $task = $request->validate([
        'title' => 'required',
        'todo' => 'required',
      ]);

      auth()->user()->tasks()->create($task);
      $tasks = Task::where('user_id', auth()->user()->id)
              ->get();

      $response = [
        'success'=>true,
        'tasks'=>$tasks,
      ];
      return response()->json($response);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $task = Task::findOrFail($id);
        $response = [
          'success' => true,
          'data' => $task,
        ];
        return response()->json($response);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
      //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
      $request->validate([
        'task_id'=>'required',
        'title'=>'required',
        'todo'=>'required',
      ]);
      $task = Task::findOrFail($request->get('task_id'));
      if($task->user_id != auth()->user()->id){
        $response = [
          'error'=>'Cannot access another user\'s tasks',
        ];
        return response()->json($response);
      }
      $task->title = $request->get('title');
      $task->todo = $request->get('todo');
      $task->save();

      $response = [
        'success'=>true,
        'updated_task'=>$task,
        'tasks'=>Task::get()->where('user_id', auth()->user()->id),
      ];
      return response()->json($response);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $task = Task::findOrFail($id);
        if($task->user_id != auth()->user()->id){
          $response = [
            'error'=>'Cannot access another user\'s tasks',
          ];
          return response()->json($response);
        }
        $task->delete();
        $alltasks = Task::all();
        $response = [
          'success'=>true,
          'tasks'=>Task::get()->where('user_id', auth()->user()->id),
        ];
        return response()->json($response);
    }
}
